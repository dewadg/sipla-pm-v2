<?php

use Illuminate\Database\Seeder;
use App\City;

class CitiesTableSeeder extends Seeder
{
    /**
     * Storing available cities.
     *
     * @var array
     */
    protected $cities = [
        ['id' => 1, 'name' => 'Kabupaten Bulungan'],
        ['id' => 2, 'name' => 'Kabupaten Malinau'],
        ['id' => 3, 'name' => 'Kabupaten Tana Tidung'],
        ['id' => 4, 'name' => 'Kabupaten Nunukan'],
        ['id' => 5, 'name' => 'Kota Tarakan'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->cities as $city) {
            if (! is_null(City::find($city['id']))) {
                continue;
            }

            City::create($city);
        }
    }
}
