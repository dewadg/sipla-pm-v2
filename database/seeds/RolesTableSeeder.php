<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    private $data = [
        [
            'id' => 1,
            'name' => 'Administrator',
        ],
        [
            'id' => 2,
            'name' => 'Operator',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $data) {
            if (! is_null(Role::find($data['id']))) {
                continue;
            }

            Role::create($data);
        }
    }
}
