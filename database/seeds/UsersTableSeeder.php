<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (is_null(User::find(1))) {
            User::create([
                'id' => 1,
                'username' => 'admin',
                'full_name' => 'Administrator',
                'password' => bcrypt('admin'),
                'role_id' => 1,
            ]);
        }

        if (is_null(User::find(2))) {
            $user = User::create([
                'id' => 2,
                'username' => 'op-bulungan',
                'full_name' => 'Operator Bulungan',
                'password' => bcrypt('op-bulungan'),
                'role_id' => 2,
            ]);

            $user->cities()->sync([1]);
        }

        if (is_null(User::find(3))) {
            $user = User::create([
                'id' => 3,
                'username' => 'op-malinau',
                'full_name' => 'Operator Malinau',
                'password' => bcrypt('op-malinau'),
                'role_id' => 2,
            ]);

            $user->cities()->sync([2]);
        }

        if (is_null(User::find(4))) {
            $user = User::create([
                'id' => 4,
                'username' => 'op-tana-tidung',
                'full_name' => 'Operator Tana Tidung',
                'password' => bcrypt('op-tana-tidung'),
                'role_id' => 2,
            ]);

            $user->cities()->sync([3]);
        }

        if (is_null(User::find(5))) {
            $user = User::create([
                'id' => 5,
                'username' => 'op-nunukan',
                'full_name' => 'Operator Nunukan',
                'password' => bcrypt('op-nunukan'),
                'role_id' => 2,
            ]);

            $user->cities()->sync([4]);
        }

        if (is_null(User::find(6))) {
            $user = User::create([
                'id' => 6,
                'username' => 'op-tarakan',
                'full_name' => 'Operator Tarakan',
                'password' => bcrypt('op-tarakan'),
                'role_id' => 2,
            ]);

            $user->cities()->sync([5]);
        }
    }
}
