<?php

use Illuminate\Database\Seeder;
use App\CompanyType;

class CompanyTypesTableSeeder extends Seeder
{
    /**
     * Storing available company types.
     *
     * @var array
     */
    protected $company_types = [
        [
            'id' => 1,
            'name' => 'Perusahaan Modal Dalam Negeri (PMDN)',
        ],
        [
            'id' => 2,
            'name' => 'Perusahaan Modal Asing (PMA)',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->company_types as $company_type) {
            if (! is_null(CompanyType::find($company_type['id']))) {
                return;
            }

            CompanyType::create($company_type);
        }
    }
}
