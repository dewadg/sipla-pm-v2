<?php

use Illuminate\Database\Seeder;
use App\ReportType;

class ReportTypesTableSeeder extends Seeder
{
    protected $report_types = [
        [
            'id' => 1,
            'name' => 'Rencana Investasi',
        ],
        [
            'id' => 2,
            'name' => 'Realisasi Investasi',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->report_types as $report_type) {
            if (! is_null(ReportType::find($report_type['id']))) {
                return;
            }

            ReportType::create($report_type);
        }
    }
}
