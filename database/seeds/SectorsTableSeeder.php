<?php

use Illuminate\Database\Seeder;
use App\Sector;

class SectorsTableSeeder extends Seeder
{
    /**
     * Storing all available sectors.
     *
     * @var array
     */
    protected $sectors = [
        ['id' => 1, 'name' => 'Tanaman Pangan dan Perkebunan'],
        ['id' => 2, 'name' => 'Peternakan'],
        ['id' => 3, 'name' => 'Kehutanan'],
        ['id' => 4, 'name' => 'Perikanan'],
        ['id' => 5, 'name' => 'Pertambangan'],
        ['id' => 6, 'name' => 'Industri Makanan'],
        ['id' => 7, 'name' => 'Industri Tekstil'],
        ['id' => 8, 'name' => 'Industri Barang Dari Kulit dan Alas Kaki'],
        ['id' => 9, 'name' => 'Industri Kayu'],
        ['id' => 10, 'name' => 'Industri Kertas dan Percetakan'],
        ['id' => 11, 'name' => 'Industri Kimia dan Farmasi'],
        ['id' => 12, 'name' => 'Industri Karet dan Plastik'],
        ['id' => 13, 'name' => 'Industri Mineral Non Logam'],
        ['id' => 14, 'name' => 'Industri Logam, Mesin dan Elektronika'],
        ['id' => 15, 'name' => 'Industri Instrumen Kedokteran, Presisi, Optik dan Jam'],
        ['id' => 16, 'name' => 'Industri Kendaraan Bermotor dan Alat Transportasi Lain'],
        ['id' => 17, 'name' => 'Listrik, Gas dan Air'],
        ['id' => 18, 'name' => 'Konstruksi'],
        ['id' => 19, 'name' => 'Perdagangan dan Reparasi'],
        ['id' => 20, 'name' => 'Hotel dan Restoran'],
        ['id' => 21, 'name' => 'Transportasi, Gudang dan Komunikasi'],
        ['id' => 22, 'name' => 'Perumahan, Kawasan Industri dan Perkantoran'],
        ['id' => 23, 'name' => 'Industri Lainnya'],        
        ['id' => 24, 'name' => 'Jasa Lainnya'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->sectors as $sector) {
            if (Sector::find($sector['id'])) {
                return;
            }

            Sector::create($sector);
        }
    }
}
