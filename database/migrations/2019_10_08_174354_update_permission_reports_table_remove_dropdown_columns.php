<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePermissionReportsTableRemoveDropdownColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permission_reports', function (Blueprint $table) {
            $table->dropForeign(['report_type_id']);
            $table->dropColumn('report_type_id');
            $table->dropColumn('quarter');
            $table->dropColumn('year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permission_reports', function (Blueprint $table) {
            $table->unsignedInteger('report_type_id');
            $table->foreign('report_type_id')
                ->references('id')->on('report_types');
            $table->smallInteger('quarter');
            $table->year('year');
        });
    }
}
