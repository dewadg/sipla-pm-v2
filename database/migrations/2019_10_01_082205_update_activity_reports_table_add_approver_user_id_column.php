<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateActivityReportsTableAddApproverUserIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activity_reports', function (Blueprint $table) {
            $table->unsignedInteger('approver_user_id')->nullable();
            $table->foreign('approver_user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activity_reports', function (Blueprint $table) {
            $table->dropForeign(['approver_user_id']);
            $table->dropColumn('approver_user_id');
        });
    }
}
