<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('report_type_id');
            $table->foreign('report_type_id')
                ->references('id')->on('report_types');
            $table->smallInteger('quarter');
            $table->year('year');
            $table->string('company_name');
            $table->text('company_address');
            $table->string('company_phone', 12);
            $table->unsignedInteger('company_type_id');
            $table->foreign('company_type_id')
                ->references('id')->on('company_types');
            $table->string('business_field');
            $table->text('location');
            $table->string('principle_permit_number')->nullable();
            $table->string('business_permit_number')->nullable();
            $table->unsignedInteger('sector_id');
            $table->foreign('sector_id')
                ->references('id')->on('sectors');
            $table->decimal('amount_of_investment', 15, 2);
            $table->unsignedInteger('national_labors');
            $table->unsignedInteger('foreign_labors');
            $table->boolean('lkpm_status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permission_reports');
    }
}
