<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('quarter');
            $table->year('year');
            $table->string('company_name');
            $table->string('permit_number');
            $table->date('date_of_registration');
            $table->unsignedInteger('city_id');
            $table->foreign('city_id')
                ->references('id')->on('cities');
            $table->unsignedInteger('company_type_id');
            $table->foreign('company_type_id')
                ->references('id')->on('company_types');
            $table->unsignedInteger('sector_id');
            $table->foreign('sector_id')
                ->references('id')->on('sectors');
            $table->decimal('additional_realization', 15, 2);
            $table->decimal('total_realization', 15, 2);
            $table->unsignedInteger('national_labors');
            $table->unsignedInteger('foreign_labors');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_reports');
    }
}
