<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePermissionReportsTableRevertQuarterYearColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permission_reports', function (Blueprint $table) {
            $table->smallInteger('quarter');
            $table->year('year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permission_reports', function (Blueprint $table) {
            $table->dropColumn('quarter');
            $table->dropColumn('year');
        });
    }
}
