## Investment Report System - Kalimantan Utara

Installation:
- Set up database variables in .env (`DB_HOST`, `DB_DATABASE`, `DB_USER`, `DB_PASSWORD`).
- Set up authentication token key in .env (`JWT_SIGN_KEY`) (Fill it with random string).
- Run `php artisan migrate`
- Run `php artisan db:seed`
