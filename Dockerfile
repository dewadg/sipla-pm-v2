FROM composer:1.10 AS php-build

WORKDIR /build

ADD . .

RUN composer install --no-dev --ignore-platform-reqs
RUN composer dump-autoload

FROM node AS js-build

WORKDIR /build

ADD . .

RUN npm i
RUN npm run prod

FROM webdevops/php-nginx:alpine-php7

WORKDIR /app

COPY --from=php-build /build .
COPY --from=js-build /build/public ./public

ENV WEB_DOCUMENT_ROOT /app/public

RUN chown -R 1000:1000 /app
RUN chmod -R 755 /app/storage

RUN php artisan route:cache
