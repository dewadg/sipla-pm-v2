<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api', 'auth']], function () {
    // User routes
    Route::get('users', 'Api\UserController@index');
    Route::post('users', 'Api\UserController@store');
    Route::get('users/{id}', 'Api\UserController@get');
    Route::patch('users/{id}', 'Api\UserController@update');
    Route::delete('users/{id}', 'Api\UserController@destroy');

    // CompanyType routes
    Route::get('company-types', 'Api\CompanyTypeController@index');

    // City routes
    Route::get('cities', 'Api\CityController@index');

    // Sector routes
    Route::get('sectors', 'Api\SectorController@index');

    // ReportType routes
    Route::get('report-types', 'Api\ReportTypeController@index');

    // PermissionReport routes
    Route::get('permission-reports', 'Api\PermissionReportController@index');
    Route::post('permission-reports', 'Api\PermissionReportController@store');
    Route::get('permission-reports/{id}', 'Api\PermissionReportController@get');
    Route::patch('permission-reports/{id}', 'Api\PermissionReportController@update');
    Route::delete('permission-reports/{id}', 'Api\PermissionReportController@destroy');

    // ActivityReport routes
    Route::get('activity-reports', 'Api\ActivityReportController@index');
    Route::post('activity-reports', 'Api\ActivityReportController@store');
    Route::get('activity-reports/{id}', 'Api\ActivityReportController@get');
    Route::patch('activity-reports/{id}', 'Api\ActivityReportController@update');
    Route::delete('activity-reports/{id}', 'Api\ActivityReportController@destroy');
    Route::post('activity-reports/{id}/verify', 'Api\ActivityReportController@verify');

    // Report Generation routes
    Route::post('stats/permission', 'Api\PermissionReportController@report');
    Route::post('stats/permission/chart', 'Api\PermissionReportController@chart');
    Route::post('stats/activity', 'Api\ActivityReportController@report');
    Route::post('stats/activity/chart', 'Api\ActivityReportController@chart');
});

Route::post('auth', 'Api\AuthController@authenticate');

Route::group(['middleware' => 'api'], function () {
    Route::post('stats/yearly', 'Api\StatsController@yearlyStats');
    Route::post('stats/quarterly', 'Api\StatsController@quarterlyStats');
    Route::post('stats/cities', 'Api\StatsController@citiesStats');

    Route::group(['prefix' => 'v2'], function () {
        Route::post('stats/yearly', 'Api\V2\StatsController@yearlyStats');
        Route::post('stats/cities', 'Api\V2\StatsController@citiesStats');
    });
});
