<?php

namespace App\Http\Middleware;

use Closure;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class FileMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty($request->get('token'))) {
            abort(500);
        }

        $token = $this->validateToken($request->get('token'));

        if (! is_null($token)) {
            $request->user = $token->getClaim('user');

            return $next($request);
        }

        abort(500);
    }

    /**
     * Validate the token.
     *
     * @param string $token
     * @return string
     */
    public function validateToken($token)
    {
        $token = (new Parser)->parse($token);
        $signer = new Sha256;

        if ($token->verify($signer, env('JWT_SIGN_KEY'))) {
            return $token;
        }

        return null;
    }
}
