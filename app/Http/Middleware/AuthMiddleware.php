<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $request->bearerToken()) {
            abort(401);
        }

        $token = $this->validateToken($request->bearerToken());
        if (! $token) {
            abort(401);
        }

        $request->user = User::with(['role', 'cities'])->findOrFail($token->getClaim('user')->id);

        return $next($request);
    }

    /**
     * Validate the token.
     *
     * @param string $token
     * @return string
     */
    public function validateToken($token)
    {
        $token = (new Parser)->parse($token);
        $signer = new Sha256;

        if ($token->verify($signer, env('JWT_SIGN_KEY'))) {
            return $token;
        }

        return null;
    }
}
