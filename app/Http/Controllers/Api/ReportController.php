<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Helpers\SpreadsheetHelper;
use App\PermissionReport;
use App\ActivityReport;

class ReportController extends Controller
{
    /**
     * Returns mandatory request validation rules.
     *
     * @return array
     */
    protected function validationRules()
    {
        return [
            'report_type_id' => 'required',
            'year' => 'required',
        ];
    }

    /**
     * Returns month-number with month-name pair.
     *
     * @param Integer $quarter
     * @return Array
     */
    private function monthsByQuarter($quarter)
    {
        switch ($quarter) {
            case 1:
                return [
                    [1, 'Januari'],
                    [2, 'Februari'],
                    [3, 'Maret'],
                ];
                break;
            case 2:
                return [
                    [4, 'April'],
                    [5, 'Mei'],
                    [6, 'Juni'],
                ];
                break;
            case 3:
                return [
                    [7, 'Juli'],
                    [8, 'Agustus'],
                    [9, 'September'],
                ];
                break;                
            case 4:
                return [
                    [10, 'Oktober'],
                    [11, 'November'],
                    [12, 'Desember'],
                ];
                break;
        }
    }

    /**
     * Fetches permission reports from DB.
     *
     * @param Array $config
     * @param Array $fields
     * @return Illuminate\Database\Collection;
     */
    private function getPermissionReport($config, $fields)
    {
        $permission_reports = PermissionReport::with([
            'type',
            'city',
            'sector',
            'companyType',
        ]);

        if (isset($config['quarter']) && $config['quarter'] != 0) {
            $permission_reports = $permission_reports
                ->whereRaw('QUARTER(date_of_approval) = ?', [$config['quarter']]);
        }

        return $permission_reports
            ->where($config)
            ->get($fields);
    }

    /**
     * Returns data for tabular view.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function permissionReport(Request $request)
    {
        $validation = Validator::make($request->all(), $this->validationRules());

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $user_data = $request->all();
        $config = [
            'year' => $user_data['year'],
        ];
        $fields = [
            'id',
            'year',
            'quarter',
            'company_name',
            'business_field',
            'principle_permit_number',
            'business_permit_number',
            'amount_of_investment',
            'national_labors',
            'foreign_labors',
            'report_type_id',
            'city_id',
            'company_type_id',
            'sector_id',
        ];

        if (isset($user_data['quarter']) && $user_data['quarter'] != 0) {
            $config['quarter'] = $user_data['quarter'];
        }

        if (isset($user_data['city_id']) && $user_data['city_id'] != 0) {
            $config['city_id'] = $user_data['city_id'];            
        }

        if (isset($user_data['sector_id']) && $user_data['sector_id'] != 0) {
            $config['sector_id'] = $user_data['sector_id'];
        }

        $config['report_type_id'] = $user_data['report_type_id'];
        $permission_reports = $this->getPermissionReport($config, $fields);

        $total_investment = 0;
        $num_of_rows = $permission_reports->count();

        $permission_reports->each(function ($report) use (&$total_investment) {
            $total_investment += $report->amount_of_investment;
        });

        return response()->json([
            'rows' => $num_of_rows,
            'total' => $total_investment,
            'items' => $permission_reports,
        ]);
    }

    /**
     * Returns data for chart.js use.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function permissionReportChart(Request $request) {
        $validation = Validator::make($request->all(), $this->validationRules());

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $user_data = $request->all();
        $config = [
            'year' => $user_data['year'],
        ];

        if (isset($user_data['quarter']) && $user_data['quarter'] != 0) {
            $config['quarter'] = $user_data['quarter'];
            $labels = collect($this->monthsByQuarter($user_data['quarter']))
                ->map(function ($item) {
                    return $item[1];
                });
        } else {
            $labels = [
                'Triwulan I',
                'Triwulan II',
                'Triwulan III',
                'Triwulan IV',
            ];
        }

        if (isset($user_data['city_id']) && $user_data['city_id'] != 0) {
            $config['city_id'] = $user_data['city_id'];
        }

        if (isset($user_data['sector_id']) && $user_data['sector_id'] != 0) {
            $config['sector_id'] = $user_data['sector_id'];
        }

        $config['report_type_id'] = $user_data['report_type_id'];
        
        if (!isset($user_data['quarter']) || $user_data['quarter'] == 0) {
            $items = $this->permissionReportAnnual($config);
        } else {
            $items = $this->permissionReportByQuarter($user_data['quarter'], $config);
        }

        return response()->json([
            'labels' => $labels,
            'items' => $items,
        ]);
    }

    /**
     * Generates annual permission report stats.
     *
     * @param Array $config
     * @return Array
     */
    private function permissionReportAnnual($config)
    {
        $permission_reports = PermissionReport::where($config)
            ->get([
                'quarter',
                'amount_of_investment'
            ])
            ->groupBy('quarter')
            ->sortBy('quarter')
            ->all();

        $items = [
            [
                isset($permission_reports[1]) ? count($permission_reports[1]) : 0,
                isset($permission_reports[1])
                    ? collect($permission_reports[1])->sum('amount_of_investment')
                    : 0,
            ],
            [
                isset($permission_reports[2]) ? count($permission_reports[2]) : 0,
                isset($permission_reports[2])
                    ? collect($permission_reports[2])->sum('amount_of_investment')
                    : 0,
            ],
            [
                isset($permission_reports[3]) ? count($permission_reports[3]) : 0,
                isset($permission_reports[3])
                    ? collect($permission_reports[3])->sum('amount_of_investment')
                    : 0,
            ],
            [
                isset($permission_reports[4]) ? count($permission_reports[4]) : 0,
                isset($permission_reports[4])
                    ? collect($permission_reports[4])->sum('amount_of_investment')
                    : 0,
            ],
        ];

        return $items;
    }

    /**
     * Generates quarter-ly permission report stats.
     *
     * @param Integer $quarter
     * @param Array $config
     * @return Array
     */
    private function permissionReportByQuarter($quarter, $config)
    {
        $config['quarter'] = $quarter;
        $items = [[0, 0], [0, 0], [0, 0]];
        $months = collect($this->monthsByQuarter($quarter))
            ->map(function ($item) {
                return $item[0];
            });

        $permission_reports = PermissionReport::where($config)
            ->get([
                'date_of_approval',
                'amount_of_investment',
            ])
            ->each(function ($item) use (&$items) {
                $items[$item->date_of_approval->month - 1][0] += 1;
                $items[$item->date_of_approval->month - 1][1] += $item->amount_of_investment;
            });

        return $items;
    }

    public function permissionReportExport(Request $request)
    {
        $validation = Validator::make($request->all(), $this->validationRules());

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $user_data = $request->all();
        $config = [
            'year' => $user_data['year'],
        ];
        $fields = [
            'id',
            'year',
            'quarter',
            'company_name',
            'business_field',
            'principle_permit_number',
            'business_permit_number',
            'amount_of_investment',
            'national_labors',
            'foreign_labors',
            'report_type_id',
            'city_id',
            'company_type_id',
            'sector_id',
        ];

        if (isset($user_data['quarter']) && $user_data['quarter'] != 0) {
            $config['quarter'] = $user_data['quarter'];
        }

        if (isset($user_data['city_id']) && $user_data['city_id'] != 0) {
            $config['city_id'] = $user_data['city_id'];            
        }

        if (isset($user_data['sector_id']) && $user_data['sector_id'] != 0) {
            $config['sector_id'] = $user_data['sector_id'];
        }

        $config['report_type_id'] = $user_data['report_type_id'];
        $total = 0;
        $data = $this->getPermissionReport($config, $fields)
            ->map(function ($item, $i) use (&$total) {
                $total += $item->amount_of_investment;

                return [
                    ($i + 1),
                    $item->company_name,
                    $item->city->name,
                    $item->type->name,
                    $item->sector->name,
                    $item->amount_of_investment,
                ];
            })
            ->all();

        array_push(
            $data,
            [
                null,
                null,
                null,
                null,
                'TOTAL',
                $total,
            ]
        );

        $report = new SpreadsheetHelper('REKAPITULASI RENCANA INVESTASI', 'Triwulan I 2018');
        $report->setContentStart('A7');
        $report->setData($data);
        $report->setColumnsWidth([
            'A' => 5,
            'B' => 30,
            'C' => 30,
            'D' => 20,
            'E' => 20,            
            'F' => 30,
        ]);

        $report->setCellValue('A6', 'No', true, true);
        $report->setCellValue('B6', 'Nama Perusahaan', true, true);
        $report->setCellValue('C6', 'Kota/Kabupaten', true, true);
        $report->setCellValue('D6', 'Jenis Izin', true, true);
        $report->setCellValue('E6', 'Sektor', true, true);
        $report->setCellValue('F6', 'Nominal', true, true);

        return $report->download();
    }

    /**
     * Fetches activity reports from DB.
     *
     * @param Array $config
     * @param Array $fields
     * @return Illuminate\Database\Collection
     */
    private function getActivityReport($config, $fields)
    {
        $activity_reports = ActivityReport::with([
            'city',
            'sector',
            'companyType',
        ]);

        if (isset($config['quarter']) && $config['quarter'] != 0) {
            $activity_reports = $activity_reports
                ->whereRaw('QUARTER(date_of_registration) = ?', [$config['quarter']]);
        }

        return $activity_reports
            ->where($config)
            ->get($fields);
    }

    /**
     * Returns data for tabular view.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function activityReport(Request $request)
    {
        $validation = Validator::make($request->all(), ['year' => 'required']);

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $user_data = $request->all();
        $config = [
            'year' => $user_data['year'],
        ];
        $fields = [
            'id',
            'year',
            'quarter',
            'company_name',
            'permit_number',
            'total_realization',
            'additional_realization',
            'national_labors',
            'foreign_labors',
            'city_id',
            'company_type_id',
            'sector_id',
        ];

        if (isset($user_data['quarter']) && $user_data['quarter'] != 0) {
            $config['quarter'] = $user_data['quarter'];
        }

        if (isset($user_data['city_id']) && $user_data['city_id'] != 0) {
            $config['city_id'] = $user_data['city_id'];
        }

        if (isset($user_data['sector_id']) && $user_data['sector_id'] != 0) {
            $config['sector_id'] = $user_data['sector_id'];
        }

        $activity_reports = $this->getActivityReport($config, $fields);

        $total_realization = 0;
        $num_of_rows = $activity_reports->count();

        $activity_reports->each(function ($report) use (&$total_realization) {
            $total_realization += $report->total_realization;
        });

        return response()->json([
            'rows' => $num_of_rows,
            'total' => $total_realization,
            'items' => $activity_reports,
        ]);
    }

    /**
     * Generates annual activity report stats.
     *
     * @param Array $config
     * @return Array
     */
    private function activityReportAnnual($config)
    {
        $activity_reports = ActivityReport::where($config)
            ->get([
                'quarter',
                'total_realization'
            ])
            ->groupBy('quarter')
            ->sortBy('quarter')
            ->all();

        $items = [
            [
                isset($activity_reports[1]) ? count($activity_reports[1]) : 0,
                isset($activity_reports[1])
                    ? collect($activity_reports[1])->sum('total_realization')
                    : 0,
            ],
            [
                isset($activity_reports[2]) ? count($activity_reports[2]) : 0,
                isset($activity_reports[2])
                    ? collect($activity_reports[2])->sum('total_realization')
                    : 0,
            ],
            [
                isset($activity_reports[3]) ? count($activity_reports[3]) : 0,
                isset($activity_reports[3])
                    ? collect($activity_reports[3])->sum('total_realization')
                    : 0,
            ],
            [
                isset($activity_reports[4]) ? count($activity_reports[4]) : 0,
                isset($activity_reports[4])
                    ? collect($activity_reports[4])->sum('total_realization')
                    : 0,
            ],
        ];

        return $items;
    }

    /**
     * Generates quarter-ly activity report stats.
     *
     * @param Integer $quarter
     * @param Array $config
     * @return Array
     */
    private function activityReportByQuarter($quarter, $config)
    {
        $config['quarter'] = $quarter;
        $items = [[0, 0], [0, 0], [0, 0]];
        $months = collect($this->monthsByQuarter($quarter))
            ->map(function ($item) {
                return $item[0];
            });

        ActivityReport::where($config)
            ->get([
                'date_of_registration',
                'total_realization',
            ])
            ->each(function ($item) use (&$items) {
                $items[$item->date_of_registration->month - 1][0] += 1;
                $items[$item->date_of_registration->month - 1][1] += $item->total_realization;
            });

        return $items;
    }

    /**
     * Returns data for chart.js use.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function activityReportChart(Request $request)
    {
        $validation = Validator::make($request->all(), ['year' => 'required']);

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $user_data = $request->all();
        $config = [
            'year' => $user_data['year'],
        ];

        if (isset($user_data['quarter']) && $user_data['quarter'] != 0) {
            $config['quarter'] = $user_data['quarter'];
            $labels = collect($this->monthsByQuarter($user_data['quarter']))
                ->map(function ($item) {
                    return $item[1];
                });
        } else {
            $labels = [
                'Triwulan I',
                'Triwulan II',
                'Triwulan III',
                'Triwulan IV',
            ];
        }

        if (isset($user_data['city_id']) && $user_data['city_id'] != 0) {
            $config['city_id'] = $user_data['city_id'];
        }

        if (isset($user_data['sector_id']) && $user_data['sector_id'] != 0) {
            $config['sector_id'] = $user_data['sector_id'];
        }

        if (!isset($user_data['quarter']) || $user_data['quarter'] == 0) {
            $items = $this->activityReportAnnual($config);
        } else {
            $items = $this->activityReportByQuarter($user_data['quarter'], $config);
        }

        return response()->json([
            'labels' => $labels,
            'items' => $items,
        ]);
    }
}
