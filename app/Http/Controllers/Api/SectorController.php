<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sector;

class SectorController extends Controller
{
    /**
     * Returns available sectors.
     *
     * @return Array
     */
    public function index()
    {
        return Sector::orderBy('name', 'ASC')->get();
    }
}
