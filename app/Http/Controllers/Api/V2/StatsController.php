<?php

namespace App\Http\Controllers\Api\V2;

use App\ActivityReport;
use App\City;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StatsController extends Controller
{
    public function yearlyStats()
    {
        $endYear = Carbon::now();
        $startYear = $endYear->copy()->subYears(3);

        $initial_data = [];
        for ($now = Carbon::now(); $now->gt($startYear); $now->subYear()) {
            $initial_data[$now->format('Y')] = 0;
        }

        $data = ActivityReport::whereNotNull('approver_user_id')
            ->whereBetween('year', [$startYear->format('Y'), $endYear->format('Y')])
            ->get()
            ->map(function ($item) {
                return [
                    'year' => $item->year,
                    'value' => $item->total_realization + $item->additional_realization,
                ];
            })
            ->filter(function ($item) use ($startYear, $endYear) {
                $start = (int) $startYear->format('Y');
                $end = (int) $endYear->format('Y');
                $item_year = (int) $item['year'];

                return $item_year >= $start && $item_year <= $end;
            })
            ->reduce(function ($carry, $item) {
                $year = $item['year'];

                $carry[$year] = $carry[$year] !== 0
                    ? $carry[$year] + $item['value']
                    : $item['value'];

                return $carry;
            }, $initial_data);

        return response()->json($data);
    }

    public function citiesStats(Request $request)
    {
        $cities = City::get()
            ->reduce(function ($carry, $item) {
                $carry[$item->name] = 0;

                return $carry;
            }, []);

        $data = ActivityReport::whereNotNull('approver_user_id')
            ->when($request->get('year'), function ($query) use ($request) {
                return $query->whereRaw('year = ?', $request->get('year'));
            })
            ->get()
            ->reduce(function ($carry, $item) {
                $carry[$item->city->name] = isset($carry[$item->city->name])
                    ? $carry[$item->city->name] + $item->total_realization + $item->additional_realization
                    : $item->total_realization + $item->additional_realization;

                return $carry;
            }, $cities);

        return response()->json($data);
    }
}
