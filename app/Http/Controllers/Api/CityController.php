<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\City;

class CityController extends Controller
{
    /**
     * Returns available cities;
     *
     * @return Array
     */
    public function index(Request $request)
    {
        if ($request->user->role_id === 1) {
            return City::get();
        } else {
            $city_ids = $request->user->cities
                ->map(function ($item) {
                    return $item->id;
                })
                ->all();

            return City::find($city_ids);
        }
    }
}
