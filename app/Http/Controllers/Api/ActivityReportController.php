<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Helpers\SpreadsheetHelper;
use App\Traits\Report;
use App\ActivityReport;

class ActivityReportController extends Controller
{
    use Report;

    /**
     * Returns available activity reports.
     *
     * @return Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return ActivityReport::with([
            'city',
            'companyType',
            'sector',
        ])->when($request->user->role_id !== 1, function ($query) use ($request) {
            $city_ids = $request->user->cities
                ->map(function ($item) {
                    return $item->id;
                })
                ->all();

            return $query->whereIn('city_id', $city_ids);
        })->get();
    }

    /**
     * Store new permission report.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'quarter' => 'required',
                'year' => 'required',
                'company_name' => 'required',
                'permit_number' => 'required',
                'date_of_registration' => 'required|date',
                'city_id' => 'required',
                'company_type_id' => 'required',
                'sector_id' => 'required',
                'additional_realization' => 'required',
                'total_realization' => 'required',
                'national_labors' => 'required',
                'foreign_labors' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->all();

            DB::transaction(function () use ($user_data) {
                ActivityReport::create($user_data);
            });

            return response()->json(null, 201);
        } catch (\Exception $e) {
            return response()->json([$e->getMessage()], 500);
        }
    }

    /**
     * Gets activity report by ID.
     *
     * @param Integer $id
     * @return Illuminate\Http\Response;
     */
    public function get($id)
    {
        $activity_report = ActivityReport::with([
            'city',
            'companyType',
            'sector',
        ])
            ->find($id);

        if (is_null($activity_report)) {
            return response()->json(null, 404);
        }

        return $activity_report;
    }

    /**
     * Update activity report.
     *
     * @param Request $request
     * @param Integer $id
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $activity_report = ActivityReport::find($id);

        if (is_null($activity_report)) {
            return response()->json(null, 404);
        }

        $validation = Validator::make(
            $request->all(),
            [
                'quarter' => 'required',
                'year' => 'required',
                'company_name' => 'required',
                'permit_number' => 'required',
                'date_of_registration' => 'required|date',
                'city_id' => 'required',
                'company_type_id' => 'required',
                'sector_id' => 'required',
                'additional_realization' => 'required',
                'total_realization' => 'required',
                'national_labors' => 'required',
                'foreign_labors' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->except('id');

            DB::transaction(function () use ($activity_report, $user_data) {
                $activity_report->update($user_data);
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()->json([$e->getMessage()], 500);
        }
    }

    /**
     * Deletes activity report by ID.
     *
     * @param Integer $id
     * @return Illuminate\Http\Response;
     */
    public function destroy($id)
    {
        $activity_report = ActivityReport::find($id);

        if (is_null($activity_report)) {
            return response()->json(null, 404);
        }

        $activity_report->delete();

        return response()->json();
    }

    /**
     * Fetches data specific for reports.
     *
     * @param Array $config
     * @param Array $fields
     * @return Illuminate\Database\Collection
     */
    private function reportData($config, $fields)
    {
        $activity_reports = ActivityReport::with([
            'city',
            'sector',
            'companyType',
        ]);

        if (isset($config['quarter']) && $config['quarter'] != 0) {
            $activity_reports = $activity_reports->where('quarter', $config['quarter']);
        }

        if (isset($config['month']) && $config['month'] != 0) {
            $activity_reports = $activity_reports->whereRaw('MONTH(date_of_registration) = ?', [$config['month']]);
            unset($config['month']);
        }

        return $activity_reports
            ->where($config)
            ->get($fields);
    }

    /**
     * Returns data for tabular view.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function report(Request $request)
    {
        $validation = Validator::make($request->all(), ['year' => 'required']);

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $user_data = $request->all();
        $config = [
            'year' => $user_data['year'],
        ];
        $fields = [
            'id',
            'year',
            'quarter',
            'company_name',
            'permit_number',
            'total_realization',
            'additional_realization',
            'national_labors',
            'foreign_labors',
            'city_id',
            'company_type_id',
            'sector_id',
        ];

        if (isset($user_data['quarter']) && $user_data['quarter'] != 0) {
            $config['quarter'] = $user_data['quarter'];
        }

        if (isset($user_data['month']) && $user_data['month'] != 0) {
            $config['month'] = $user_data['month'];
        }

        if (isset($user_data['city_id']) && $user_data['city_id'] != 0) {
            $config['city_id'] = $user_data['city_id'];
        }

        if (isset($user_data['sector_id']) && $user_data['sector_id'] != 0) {
            $config['sector_id'] = $user_data['sector_id'];
        }

        $activity_reports = $this->reportData($config, $fields);

        $total_realization = 0;
        $num_of_rows = $activity_reports->count();

        $activity_reports->each(function ($report) use (&$total_realization) {
            $total_realization += $report->total_realization;
        });

        return response()->json([
            'rows' => $num_of_rows,
            'total' => $total_realization,
            'items' => $activity_reports,
        ]);
    }

    /**
     * Generates annually activity report stats.
     *
     * @param Array $config
     * @return Array
     */
    private function annually($config)
    {
        $activity_reports = ActivityReport::where($config)
            ->get([
                'quarter',
                'total_realization'
            ])
            ->groupBy('quarter')
            ->sortBy('quarter')
            ->all();

        $items = [
            [
                isset($activity_reports[1]) ? count($activity_reports[1]) : 0,
                isset($activity_reports[1])
                    ? collect($activity_reports[1])->sum('total_realization')
                    : 0,
            ],
            [
                isset($activity_reports[2]) ? count($activity_reports[2]) : 0,
                isset($activity_reports[2])
                    ? collect($activity_reports[2])->sum('total_realization')
                    : 0,
            ],
            [
                isset($activity_reports[3]) ? count($activity_reports[3]) : 0,
                isset($activity_reports[3])
                    ? collect($activity_reports[3])->sum('total_realization')
                    : 0,
            ],
            [
                isset($activity_reports[4]) ? count($activity_reports[4]) : 0,
                isset($activity_reports[4])
                    ? collect($activity_reports[4])->sum('total_realization')
                    : 0,
            ],
        ];

        return $items;
    }

    /**
     * Generates quarterly activity report stats.
     *
     * @param Integer $quarter
     * @param Array $config
     * @return Array
     */
    private function quarterly($quarter, $config)
    {
        $config['quarter'] = $quarter;
        $items = [[0, 0], [0, 0], [0, 0]];
        $months = collect($this->monthsByQuarter($quarter))
            ->map(function ($item) {
                return $item[0];
            });

        ActivityReport::where($config)
            ->get([
                'date_of_registration',
                'total_realization',
            ])
            ->each(function ($item) use (&$items) {
                $items[$this->getMonthsIndex($item->date_of_registration->month)][0] += 1;
                $items[$this->getMonthsIndex($item->date_of_registration->month)][1] += $item->total_realization;
            });

        return $items;
    }

    /**
     * Generates monthly permission report stats.
     *
     * @param Integer $quarter
     * @param Array $config
     * @return Array
     */
    private function monthly($month, $config)
    {
        return ActivityReport::where($config)
            ->get([
                'date_of_registration',
                'total_realization',
            ])
            ->filter(function ($item) use ($month) {
                return $item->date_of_registration->month === $month;
            })
            ->reduce(function ($carry, $item) {
                $carry[0][0] += 1;
                $carry[0][1] += $item->total_realization;

                return $carry;
            }, [[0, 0]]);
    }

    /**
     * Returns data for chart.js use.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function chart(Request $request)
    {
        $validation = Validator::make($request->all(), ['year' => 'required']);

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $user_data = $request->all();
        $config = [
            'year' => $user_data['year'],
        ];

        if (isset($user_data['quarter']) && $user_data['quarter'] != 0) {
            $config['quarter'] = $user_data['quarter'];
            $labels = collect($this->monthsByQuarter($user_data['quarter']))
                ->map(function ($item) {
                    return $item[1];
                });
        } else if (isset($user_data['month']) && $user_data['month'] != 0) {
            $labels = [$this->getMonthName($user_data['month'])];
        } else {
            $labels = [
                'Triwulan I',
                'Triwulan II',
                'Triwulan III',
                'Triwulan IV',
            ];
        }

        if (isset($user_data['city_id']) && $user_data['city_id'] != 0) {
            $config['city_id'] = $user_data['city_id'];
        }

        if (isset($user_data['sector_id']) && $user_data['sector_id'] != 0) {
            $config['sector_id'] = $user_data['sector_id'];
        }

        if (isset($user_data['quarter']) && $user_data['quarter'] != 0) {
            $items = $this->quarterly($user_data['quarter'], $config);
        } else if (isset($user_data['month']) && $user_data['month'] != 0) {
            $items = $this->monthly($user_data['month'], $config);
        } else {
            $items = $this->annually($config);
        }

        return response()->json([
            'labels' => $labels,
            'items' => $items,
        ]);
    }

    public function export(Request $request)
    {
        $validation = Validator::make($request->all(), ['year' => 'required']);

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $user_data = $request->all();
        $config = [
            'year' => $user_data['year'],
        ];
        $fields = [
            'id',
            'year',
            'quarter',
            'company_name',
            'permit_number',
            'total_realization',
            'additional_realization',
            'national_labors',
            'foreign_labors',
            'city_id',
            'company_type_id',
            'sector_id',
        ];

        if (isset($user_data['quarter']) && $user_data['quarter'] != 0) {
            $config['quarter'] = $user_data['quarter'];
            $subtitle = 'Triwulan ' . $config['quarter']
                . ' - Dicetak pada ' . date('Y-m-d H:i:s');
        } else if (isset($user_data['month']) && $user_data['month'] != 0) {
            $config['month'] = $user_data['month'];
            $subtitle = 'Bulan ' . $this->getMonthName($user_data['month'])
                . ' - Dicetak pada ' . date('Y-m-d H:i:s');
        } else {
            $subtitle = 'Tahun ' . $config['year']
                . ' - Dicetak pada ' . date('Y-m-d H:i:s');
        }

        if (isset($user_data['city_id']) && $user_data['city_id'] != 0) {
            $config['city_id'] = $user_data['city_id'];            
        }

        if (isset($user_data['sector_id']) && $user_data['sector_id'] != 0) {
            $config['sector_id'] = $user_data['sector_id'];
        }

        $total = 0;
        $additional_total = 0;
        $total_national_labors = 0;
        $total_foreign_labors = 0;
        $data = $this->reportData($config, $fields)
            ->map(function ($item, $i) use (
                &$total,
                &$total_national_labors,
                &$total_foreign_labors,
                &$additional_total
            ) {
                $total += $item->total_realization;
                $additional_total += $item->additional_realization;
                $total_national_labors += $item->national_labors;
                $total_foreign_labors += $item->foreign_labors;

                return [
                    ($i + 1),
                    $item->company_name,
                    $item->city->name,
                    $item->companyType->name,
                    $item->permit_number,
                    $item->sector->name,
                    $item->total_realization,
                    $item->additional_realization,
                    $item->national_labors,
                    $item->foreign_labors,
                ];
            })
            ->all();

        array_push(
            $data,
            [
                null,
                null,
                null,
                null,
                null,
                'TOTAL',
                $total,
                $additional_total,
                $total_national_labors,
                $total_foreign_labors,
            ]
        );

        $report = new SpreadsheetHelper('REKAPITULASI LKPM', $subtitle);
        $report->setContentStart('A7');
        $report->setData($data);
        $report->setColumnsWidth([
            'A' => 5,
            'B' => 30,
            'C' => 30,
            'D' => 20,
            'E' => 20,            
            'F' => 30,
            'G' => 30,
        ]);

        $report->setCellValue('A6', 'No', true, true);
        $report->setCellValue('B6', 'Nama Perusahaan', true, true);
        $report->setCellValue('C6', 'Kota/Kabupaten', true, true);
        $report->setCellValue('D6', 'Jenis PM', true, true);
        $report->setCellValue('E6', 'Nomor Izin/NIB', true, true);
        $report->setCellValue('F6', 'Sektor', true, true);
        $report->setCellValue('G6', 'Rencana Investasi', true, true);
        $report->setCellValue('H6', 'Capaian Realisasi', true, true);
        $report->setCellValue('I6', 'TKI', true, true);
        $report->setCellValue('J6', 'TKA', true, true);

        return $report->download();
    }

    public function verify(Request $request, $id)
    {
        $activity_report = ActivityReport::find($id);

        if (is_null($activity_report)) {
            return response()->json(null, 404);
        }

        $activity_report->approver_user_id = $request->user->id;
        $activity_report->save();

        return response()->json();
    }
}
