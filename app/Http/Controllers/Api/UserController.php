<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    /**
     * Returns all available users.
     *
     * @return Array
     */
    public function index()
    {
        return User::get();
    }

    /**
     * Store a new user.
     *
     * @param Request $request
     * @return Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'username' => 'required|unique:users,username',
                'full_name' => 'required',
                'password' => 'required',
                'password_conf' => 'required|same:password',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->all();
            $user_data['password'] = bcrypt($user_data['password']);

            DB::transaction(function () use ($user_data) {
                User::create($user_data);
            });

            return response()->json(null, 201);
        } catch (\Exception $e) {
            return response()->json([$e->getMessage()], 500);
        }
    }

    /**
     * Get user by ID.
     *
     * @param Integer $id
     * @return User
     */
    public function get($id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            return response()->json(null, 404);
        }

        return $user;
    }

    /**
     * Update a user.
     *
     * @param Request $request
     * @param Integer $id
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            return response()->json(null, 404);
        }

        $validation = Validator::make(
            $request->all(),
            [
                'full_name' => 'required',
                'password' => 'sometimes|required',
                'password_conf' => 'required_with:password|same:password',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->all();

            if (isset($user_data['password'])) {
                $user_data['password'] = bcrypt($user_data['password']);
            }

            DB::transaction(function () use ($user, $user_data) {
                $user->update($user_data);
            });

            return response()->json(null, 200);
        } catch (\Exception $e) {
            return response()->json([$e->getMessage()], 500);
        }
    }

    /**
     * Delete a user.
     *
     * @param Integer $id
     * @return void
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if (is_null($user)) {
            return response()->json(null, 404);
        }

        $user->delete();

        return response()->json();
    }
}
