<?php

namespace App\Http\Controllers\Api;

use App\ActivityReport;
use App\City;
use App\Http\Controllers\Controller;

class StatsController extends Controller
{
    public function yearlyStats()
    {
        $data = ActivityReport::whereNotNull('approver_user_id')
            ->get()
            ->map(function ($item) {
                return [
                    'year' => $item->date_of_registration->format('Y'),
                    'value' => $item->total_realization + $item->additional_realization,
                ];
            })
            ->reduce(function ($carry, $item) {
                $carry[$item['year']] = isset($carry[$item['year']])
                    ? $carry[$item['year']] + $item['value']
                    : $item['value'];

                return $carry;
            }, []);

        return response()->json($data);
    }

    public function quarterlyStats()
    {
        $quarter = [
            'Triwulan I' => 0,
            'Triwulan II' => 0,
            'Triwulan III' => 0,
            'Triwulan IV' => 0,
        ];

        $data = ActivityReport::whereNotNull('approver_user_id')
            ->get()
            ->reduce(function ($carry, $item) {
                $index = 'Triwulan I';
                switch ($item->quarter) {
                    case 1:
                        $index = 'Triwulan I';
                        break;
                    case 2:
                        $index = 'Triwulan II';
                        break;
                    case 3:
                        $index = 'Triwulan III';
                        break;
                    case 4:
                        $index = 'Triwulan 4';
                        break;
                }

                $carry[$index] = isset($carry[$index])
                    ? $carry[$index] + $item->total_realization + $item->additional_realization
                    : $item->total_realization + $item->additional_realization;

                return $carry;
            }, []);

        return response()->json(array_merge($quarter, $data));
    }

    public function citiesStats()
    {
        $cities = City::get()
            ->reduce(function ($carry, $item) {
                $carry[$item->name] = 0;

                return $carry;
            }, []);

        $data = ActivityReport::whereNotNull('approver_user_id')
            ->get()
            ->reduce(function ($carry, $item) {
                $carry[$item->city->name] = isset($carry[$item->city->name])
                    ? $carry[$item->city->name] + $item->total_realization + $item->additional_realization
                    : $item->total_realization + $item->additional_realization;

                return $carry;
            }, []);

        return response()->json(array_merge($cities, $data));
    }
}
