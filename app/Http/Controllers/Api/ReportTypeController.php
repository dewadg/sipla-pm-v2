<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ReportType;

class ReportTypeController extends Controller
{
    /**
     * Returns available report types.
     *
     * @return void
     */
    public function index()
    {
        return ReportType::get();
    }
}
