<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CompanyType;

class CompanyTypeController extends Controller
{
    /**
     * Returns available company types.
     *
     * @return Array
     */
    public function index()
    {
        return CompanyType::orderBy('name', 'ASC')->get();
    }
}
