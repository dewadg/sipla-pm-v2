<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Helpers\SpreadsheetHelper;
use App\Traits\Report;
use App\PermissionReport;

class PermissionReportController extends Controller
{
    use Report;

    /**
     * Returns available permission reports.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return PermissionReport::with([
            'type',
            'companyType',
            'city',
            'sector',
        ])
            ->get();
    }

    /**
     * Store new permission report.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'company_name' => 'required',
                'company_phone' => 'required|max:12',
                'company_address' => 'required',
                'company_type_id' => 'required',
                'business_field' => 'required',
                'location' => 'required',
                'city_id' => 'required',
                // 'principle_permit_number' => 'required_if:report_type_id,1',
                // 'business_permit_number' => 'required_if:report_type_id,2',
                'sector_id' => 'required',
                'amount_of_investment' => 'required',
                'national_labors' => 'required',
                'foreign_labors' => 'required',
                'lkpm_status' => 'required|boolean',
                'date_of_approval' => 'required|date',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->all();

            DB::transaction(function () use ($user_data) {
                PermissionReport::create($user_data);
            });

            return response()->json(null, 201);
        } catch (\Exception $e) {
            return response()->json([$e->getMessage()], 500);
        }
    }
    
    /**
     * Gets permission report by ID.
     *
     * @param Integer $id
     * @return Illuminate\Http\Response;
     */
    public function get($id)
    {
        $permission_report = PermissionReport::with([
            'type',
            'companyType',
            'city',
            'sector',
        ])
            ->find($id);

        if (is_null($permission_report)) {
            return response()->json(null, 404);
        }

        return $permission_report;
    }

    /**
     * Updates permission report.
     *
     * @param Request $request
     * @param Integer $id
     * @return Illuminate\Http\Response;
     */
    public function update(Request $request, $id)
    {
        $permission_report = PermissionReport::find($id);

        if (is_null($permission_report)) {
            return response()->json(null, 404);
        }

        $validation = Validator::make(
            $request->all(),
            [
                'company_name' => 'required',
                'company_phone' => 'required|max:12',
                'company_address' => 'required',
                'company_type_id' => 'required',
                'business_field' => 'required',
                'location' => 'required',
                'city_id' => 'required',
                // 'principle_permit_number' => 'required_if:report_type_id,1',
                // 'business_permit_number' => 'required_if:report_type_id,2',
                'sector_id' => 'required',
                'amount_of_investment' => 'required',
                'national_labors' => 'required',
                'foreign_labors' => 'required',
                'lkpm_status' => 'required|boolean',
                'date_of_approval' => 'required|date',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        try {
            $user_data = $request->except('id');

            DB::transaction(function () use ($permission_report, $user_data) {
                $permission_report->update($user_data);
            });

            return response()->json();
        } catch (\Exception $e) {
            return response()->json([$e->getMessage()], 500);
        }
    }

    /**
     * Deletes permission report by ID.
     *
     * @param Integer $id
     * @return Illuminate\Http\Response;
     */
    public function destroy($id)
    {
        $permission_report = PermissionReport::find($id);

        if (is_null($permission_report)) {
            return response()->json(null, 404);
        }

        $permission_report->delete();

        return response()->json();
    }

    /**
     * Fetches data specific for reports.
     *
     * @param Array $config
     * @param Array $fields
     * @return Illuminate\Database\Collection;
     */
    private function reportData($config, $fields)
    {
        $permission_reports = PermissionReport::with([
            'type',
            'city',
            'sector',
            'companyType',
        ]);

        if (isset($config['quarter']) && $config['quarter'] != 0) {
            $permission_reports = $permission_reports->where('quarter', $config['quarter']);
        }

        if (isset($config['month']) && $config['month'] != 0) {
            $permission_reports = $permission_reports->whereRaw('MONTH(date_of_approval) = ?', [$config['month']]);
            unset($config['month']);
        }

        return $permission_reports
            ->where($config)
            ->get($fields);
    }

    /**
     * Returns data for tabular report.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function report(Request $request)
    {
        $validation = Validator::make(
            $request->all(), 
            [
                // 'report_type_id' => 'required',
                'year' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $user_data = $request->all();
        $config = [
            'year' => $user_data['year'],
        ];
        $fields = [
            'company_name',
            'business_field',
            'principle_permit_number',
            'business_permit_number',
            'amount_of_investment',
            'national_labors',
            'foreign_labors',
            // 'report_type_id',
            'city_id',
            'company_type_id',
            'sector_id',
        ];

        if (isset($user_data['quarter']) && $user_data['quarter'] != 0) {
            $config['quarter'] = $user_data['quarter'];
        }

        if (isset($user_data['month']) && $user_data['month'] != 0) {
            $config['month'] = $user_data['month'];
        }

        if (isset($user_data['city_id']) && $user_data['city_id'] != 0) {
            $config['city_id'] = $user_data['city_id'];            
        }

        if (isset($user_data['sector_id']) && $user_data['sector_id'] != 0) {
            $config['sector_id'] = $user_data['sector_id'];
        }

        // $config['report_type_id'] = $user_data['report_type_id'];
        $permission_reports = $this->reportData($config, $fields);

        $total_investment = 0;
        $num_of_rows = $permission_reports->count();

        $permission_reports->each(function ($report) use (&$total_investment) {
            $total_investment += $report->amount_of_investment;
        });

        return response()->json([
            'rows' => $num_of_rows,
            'total' => $total_investment,
            'items' => $permission_reports,
        ]);
    }

    /**
     * Generates annually permission report stats.
     *
     * @param Array $config
     * @return Array
     */
    private function annually($config)
    {
        $permission_reports = PermissionReport::where($config)
            ->get([
                'quarter',
                'amount_of_investment'
            ])
            ->groupBy('quarter')
            ->sortBy('quarter')
            ->all();

        $items = [
            [
                isset($permission_reports[1]) ? count($permission_reports[1]) : 0,
                isset($permission_reports[1])
                    ? collect($permission_reports[1])->sum('amount_of_investment')
                    : 0,
            ],
            [
                isset($permission_reports[2]) ? count($permission_reports[2]) : 0,
                isset($permission_reports[2])
                    ? collect($permission_reports[2])->sum('amount_of_investment')
                    : 0,
            ],
            [
                isset($permission_reports[3]) ? count($permission_reports[3]) : 0,
                isset($permission_reports[3])
                    ? collect($permission_reports[3])->sum('amount_of_investment')
                    : 0,
            ],
            [
                isset($permission_reports[4]) ? count($permission_reports[4]) : 0,
                isset($permission_reports[4])
                    ? collect($permission_reports[4])->sum('amount_of_investment')
                    : 0,
            ],
        ];

        return $items;
    }

    /**
     * Generates quarterly permission report stats.
     *
     * @param Integer $quarter
     * @param Array $config
     * @return Array
     */
    private function quarterly($quarter, $config)
    {
        $config['quarter'] = $quarter;
        $items = [[0, 0], [0, 0], [0, 0]];

        PermissionReport::where($config)
            ->get([
                'date_of_approval',
                'amount_of_investment',
            ])
            ->each(function ($item) use (&$items, $quarter) {
                $items[$this->getMonthsIndex($item->date_of_approval->month)][0] += 1;
                $items[$this->getMonthsIndex($item->date_of_approval->month)][1] += $item->amount_of_investment;
            });

        return $items;
    }

    /**
     * Generates monthly permission report stats.
     *
     * @param Integer $quarter
     * @param Array $config
     * @return Array
     */
    private function monthly($month, $config)
    {
        return PermissionReport::where($config)
            ->get([
                'date_of_approval',
                'amount_of_investment',
            ])
            ->filter(function ($item) use ($month) {
                return $item->date_of_approval->month === $month;
            })
            ->reduce(function ($carry, $item) {
                $carry[0][0] += 1;
                $carry[0][1] += $item->amount_of_investment;

                return $carry;
            }, [[0, 0]]);
    }

    /**
     * Returns data for chart.js.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function chart(Request $request) {
        $validation = Validator::make(
            $request->all(), 
            [
                // 'report_type_id' => 'required',
                'year' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $user_data = $request->all();
        $config = [
            'year' => $user_data['year'],
        ];

        if (isset($user_data['quarter']) && $user_data['quarter'] != 0) {
            $config['quarter'] = $user_data['quarter'];
            $labels = collect($this->monthsByQuarter($user_data['quarter']))
                ->map(function ($item) {
                    return $item[1];
                });
        } else if (isset($user_data['month']) && $user_data['month'] != 0) {
            $labels = [$this->getMonthName($user_data['month'])];
        } else {
            $labels = [
                'Triwulan I',
                'Triwulan II',
                'Triwulan III',
                'Triwulan IV',
            ];
        }

        if (isset($user_data['city_id']) && $user_data['city_id'] != 0) {
            $config['city_id'] = $user_data['city_id'];
        }

        if (isset($user_data['sector_id']) && $user_data['sector_id'] != 0) {
            $config['sector_id'] = $user_data['sector_id'];
        }

        // $config['report_type_id'] = $user_data['report_type_id'];
        
        if (isset($user_data['quarter']) && $user_data['quarter'] != 0) {
            $items = $this->quarterly($user_data['quarter'], $config);
        } else if (isset($user_data['month']) && $user_data['month'] != 0) {
            $items = $this->monthly($user_data['month'], $config);
        } else {
            $items = $this->annually($config);
        }

        return response()->json([
            'labels' => $labels,
            'items' => $items,
        ]);
    }

    /**
     * Exports report to XLSX.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function export(Request $request)
    {
        $validation = Validator::make(
            $request->all(), 
            [
                // 'report_type_id' => 'required',
                'year' => 'required',
            ]
        );

        if ($validation->fails()) {
            return response()->json($validation->errors(), 422);
        }

        $user_data = $request->all();
        $config = [
            'year' => $user_data['year'],
        ];
        $fields = [
            'company_name',
            'location',
            'business_field',
            'principle_permit_number',
            'business_permit_number',
            'amount_of_investment',
            'national_labors',
            'foreign_labors',
            // 'report_type_id',
            'city_id',
            'company_type_id',
            'sector_id',
        ];

        if (isset($user_data['quarter']) && $user_data['quarter'] != 0) {
            $config['quarter'] = $user_data['quarter'];
            $subtitle = 'Triwulan ' . $config['quarter']
                . ' - Dicetak pada ' . date('Y-m-d H:i:s');
        } else if (isset($user_data['month']) && $user_data['month'] != 0) {
            $config['month'] = $user_data['month'];
            $subtitle = 'Bulan ' . $this->getMonthName($user_data['month'])
                . ' - Dicetak pada ' . date('Y-m-d H:i:s');;
        } else {
            $subtitle = 'Tahun ' . $config['year']
                . ' - Dicetak pada ' . date('Y-m-d H:i:s');
        }

        if (isset($user_data['city_id']) && $user_data['city_id'] != 0) {
            $config['city_id'] = $user_data['city_id'];            
        }

        if (isset($user_data['sector_id']) && $user_data['sector_id'] != 0) {
            $config['sector_id'] = $user_data['sector_id'];
        }

        // $config['report_type_id'] = $user_data['report_type_id'];
        $total = 0;
        $total_national_labors = 0;
        $total_foreign_labors = 0;
        $data = $this->reportData($config, $fields)
            ->map(function ($item, $i) use (&$total, &$total_national_labors, &$total_foreign_labors) {
                $total += $item->amount_of_investment;
                $total_national_labors += $item->national_labors;
                $total_foreign_labors += $item->foreign_labors;

                return [
                    ($i + 1),
                    $item->company_name,
                    $item->city->name,
                    $item->location,
                    $item->sector->name,
                    $item->amount_of_investment,
                    $item->national_labors,
                    $item->foreign_labors,
                ];
            })
            ->all();

        array_push(
            $data,
            [
                null,
                null,
                null,
                null,
                'TOTAL',
                $total,
                $total_national_labors,
                $total_foreign_labors,
            ]
        );

        $report = new SpreadsheetHelper('REKAPITULASI RENCANA INVESTASI', $subtitle);
        $report->setContentStart('A7');
        $report->setData($data);
        $report->setColumnsWidth([
            'A' => 5,
            'B' => 30,
            'C' => 30,
            'D' => 20,
            'E' => 20,
            'F' => 30,
        ]);

        $report->setCellValue('A6', 'No', true, true);
        $report->setCellValue('B6', 'Nama Perusahaan', true, true);
        $report->setCellValue('C6', 'Kota/Kabupaten', true, true);
        $report->setCellValue('D6', 'Lokasi Proyek', true, true);
        $report->setCellValue('E6', 'Sektor', true, true);
        $report->setCellValue('F6', 'Rencana Investasi', true, true);
        $report->setCellValue('G6', 'TKI', true, true);
        $report->setCellValue('H6', 'TKA', true, true);

        return $report->download();
    }
}
