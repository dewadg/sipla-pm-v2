<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Display the homepage
     *
     * @return Illuminate\View\View
     */
    public function homepage()
    {
        return view('index');
    }
}
