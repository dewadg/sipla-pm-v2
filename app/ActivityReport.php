<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityReport extends Model
{
    use SoftDeletes;

    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quarter',
        'year',
        'company_name',
        'permit_number',
        'date_of_registration',
        'city_id',
        'company_type_id',
        'business_field',
        'sector_id',
        'additional_realization',
        'total_realization',
        'national_labors',
        'foreign_labors',
    ];

    protected $dates = [
        'deleted_at',
        'date_of_registration',
    ];

    /**
     * Casts attributes from DB.
     *
     * @var array
     */
    protected $casts = [
        'total_realization' => 'double',
        'additional_realization' => 'double',
    ];

    /**
     * One-to-one relationship to City.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * One-to-one relationship to CompanyType.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function companyType()
    {
        return $this->belongsTo(CompanyType::class);
    }

    /**
     * One-to-one relationship to Sector.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sector()
    {
        return $this->belongsTo(Sector::class);
    }
}
