<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as Writer;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class SpreadsheetHelper
{
    /**
     * PhpSpreadsheet Spreadsheet instance.
     *
     * @var Spreadsheet
     */
    protected $spreadsheet;

    /**
     * PhpSpreadsheet Xlsx writer instance.
     *
     * @var Writer
     */
    protected $writer;

    /**
     * Stores the data.
     *
     * @var array
     */
    protected $data;

    /**
     * Stores the content coordinate start.
     *
     * @var string
     */
    protected $start = 'A1';

    /**
     * Constructs the helper.
     */
    public function __construct($title, $subtitle)
    {
        $this->spreadsheet = new Spreadsheet;
        $this->writer = new Writer($this->spreadsheet);

        $this->setHeader($title, $subtitle);
    }

    /**
     * Sets the content start.
     *
     * @param string $coordinate
     * @return void
     */
    public function setContentStart($coordinate)
    {
        $this->start = $coordinate;
    }

    /**
     * Sets logo.
     *
     * @param string $path
     * @param Spreadsheet $sheet
     * @return void
     */
    protected function setLogo($path, $sheet)
    {
        $logo = new Drawing;
        $logo->setName('logo');
        $logo->setDescription('Provinsi Kalimantan Utara');
        $logo->setPath(resource_path($path));
        $logo->setCoordinates('A1');
        $logo->setHeight(80);
        
        $logo->setWorksheet($sheet);
    }

    /**
     * Sets header of the Spreadsheet file.
     *
     * @return void
     */
    protected function setHeader($first, $second = null)
    {
        $sheet = $this->spreadsheet->getActiveSheet();

        $sheet->mergeCells('A1:F1');
        $sheet->setCellValue('A1', 'PEMERINTAH PROVINSI KALIMANTAN UTARA');
        $sheet->mergeCells('A2:F2');
        $sheet->setCellValue('A2', 'DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU');
        $sheet->mergeCells('A3:F3');
        $sheet->setCellValue('A3', $first);
        $sheet->mergeCells('A4:F4');
        $sheet->setCellValue('A4', $second);
        $sheet->getStyle('A1:F4')->getFont()->setBold(true);
        $sheet->getStyle('A1:F4')->applyFromArray([
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
            ],
        ]);

        $this->setLogo('assets/img/emblem.png', $sheet);
    }

    /**
     * Sets column width.
     *
     * @param array $columns
     * @return void
     */
    public function setColumnsWidth($columns)
    {
        $sheet = $this->spreadsheet->getActiveSheet();
        
        foreach ($columns as $column => $width) {
            $sheet->getColumnDimension($column)->setWidth($width);
        }
    }

    /**
     * Sets data.
     *
     * @param array $data
     * @return void
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Sets cell value.
     *
     * @param string $coordinate
     * @param mixed $value
     * @return void
     */
    public function setCellValue($coordinate, $value, $bold = false, $centered = false)
    {
        $sheet = $this->spreadsheet->getActiveSheet();
        $sheet->setCellValue($coordinate, $value);

        if ($bold) {
            $sheet->getStyle($coordinate)->getFont()->setBold(true);
        }

        if ($centered) {
            $sheet->getStyle($coordinate)->applyFromArray([
                'alignment' => [
                    'horizontal' => Alignment::HORIZONTAL_CENTER,
                ],
            ]);
        }
    }

    /**
     * Generates temporary file and returns its name.
     *
     * @return string
     */
    protected function tempFile()
    {
        return tempnam(sys_get_temp_dir(), 'file_' . date('Y-m-d_H:i:s'));
    }

    /**
     * Writes the data into Spreadsheet.
     *
     * @return string
     */
    protected function write()
    {
        $sheet = $this->spreadsheet->getActiveSheet();
        $file_name = $this->tempFile() . '.xlsx';

        $sheet->fromArray(
            $this->data,
            null,
            $this->start,
            false
        );
        $this->writer->save($file_name);

        return $file_name;
    }

    /**
     * Returns response as file download.
     *
     * @return Illuminate\Http\Response;
     */
    public function download()
    {
        $file = $this->write();

        return response()->download($file);
    }
}
