<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillables = [
        'id',
        'name',
    ];
}
