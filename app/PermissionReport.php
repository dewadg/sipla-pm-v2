<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionReport extends Model
{
    /**
     * Attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quarter',
        'year',
        'company_name',
        'company_phone',
        'company_address',
        'company_type_id',
        'business_field',
        'location',
        'city_id',
        'principle_permit_number',
        'business_permit_number',
        'sector_id',
        'amount_of_investment',
        'national_labors',
        'foreign_labors',
        'lkpm_status',
        'date_of_approval',
    ];

    /**
     * Casts attributes from DB.
     *
     * @var array
     */
    protected $casts = [
        'lkpm_status' => 'boolean',
        'amount_of_investment' => 'double',
    ];

    /**
     * Date-casted attributes.
     *
     * @var array
     */
    protected $dates = [
        'date_of_approval',
    ];

    /**
     * One-to-one relationship to ReportType.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(ReportType::class, 'report_type_id');
    }

    /**
     * One-to-one relationship to CompanyType.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function companyType()
    {
        return $this->belongsTo(CompanyType::class);
    }

    /**
     * One-to-one relationship to City.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * One-to-one relationship to Sector.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sector()
    {
        return $this->belongsTo(Sector::class);
    }
}
