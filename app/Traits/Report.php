<?php

namespace App\Traits;

trait Report
{
    /**
     * Returns month-number with month-name pair.
     *
     * @param Integer $quarter
     * @return Array
     */
    private function monthsByQuarter($quarter)
    {
        switch ($quarter) {
            case 1:
                return [
                    [1, 'Januari'],
                    [2, 'Februari'],
                    [3, 'Maret'],
                ];
                break;
            case 2:
                return [
                    [4, 'April'],
                    [5, 'Mei'],
                    [6, 'Juni'],
                ];
                break;
            case 3:
                return [
                    [7, 'Juli'],
                    [8, 'Agustus'],
                    [9, 'September'],
                ];
                break;
            case 4:
                return [
                    [10, 'Oktober'],
                    [11, 'November'],
                    [12, 'Desember'],
                ];
                break;
        }
    }

    public function getMonthsIndex($month)
    {
        switch ($month) {
            case 1:
            case 4:
            case 7:
            case 10:
                return 0;
            case 2:
            case 5:
            case 8:
            case 11:
                return 1;
            case 3:
            case 6:
            case 9:
            case 12:
                return 2;
        }
    }

    public function getMonthName($index)
    {
        switch ($index) {
            case 1:
                return 'Januari';
            case 2:
                return 'Februari';
            case 3:
                return 'Maret';
            case 4:
                return 'April';
            case 5:
                return 'Mei';
            case 6:
                return 'Juni';
            case 7:
                return 'Juli';
            case 8:
                return 'Agustus';
            case 9:
                return 'September';
            case 10:
                return 'Oktober';
            case 11:
                return 'November';
            case 12:
                return 'Desember';
        }
    }
}
