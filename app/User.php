<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'username',
        'full_name',
        'password',
        'role_id',
        'approver_user_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJwt()
    {
        $data = $this;
        $data->cities = $this->cities;
        $signer = new Sha256;
        $token = (new Builder)
            ->setIssuer(env('APP_URL'))
            ->setAudience((env('APP_URL')))
            ->setIssuedAt(time())
            ->set('user', $data)
            ->sign($signer, env('JWT_SIGN_KEY'))
            ->getToken();

        return (string) $token;
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function cities()
    {
        return $this->belongsToMany(City::class);
    }
}
