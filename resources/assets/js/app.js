import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuelidate from 'vuelidate';
import Http from './services/Http';
import App from './components/App';
import router from './routes';
import store from './stores';

Vue.use(Vuetify);
Vue.use(Vuelidate);

Http.init();

new Vue({
  router,
  store,

  render(h) {
    return h(App);
  },
})
  .$mount('#app');
