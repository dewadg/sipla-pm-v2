import Http from '../services/Http';

const state = {
  data: [],
};

const mutations = {
  setSource(state, source) {
    state.data = source;
  },
};

const actions = {
  fetchAll(context) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        context.commit('setSource', res.data);
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);

        reject(errData);
      };

      Http.get('activity-reports', successCallback, errorCallback);
    });
  },

  store(context, payload) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        if (res.status === 201) {
          resolve();
        }
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);

        reject(errData);
      };

      Http.post('activity-reports', payload, successCallback, errorCallback);
    });
  },

  fetch(context, id) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        resolve(res.data);
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);

        reject(errData);
      };

      Http.get(`activity-reports/${id}`, successCallback, errorCallback);
    });
  },

  update(context, payload) {
    return new Promise((resolve, reject) => {
      const { id } = payload;

      const successCallback = () => {
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);

        reject(errData);
      };

      Http.patch(`activity-reports/${id}`, payload, successCallback, errorCallback);
    });
  },

  delete(context, id) {
    return new Promise((resolve, reject) => {
      const successCallback = () => {
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);

        reject(errData);
      };

      Http.delete(`activity-reports/${id}`, successCallback, errorCallback);
    });
  },

  verify(context, id) {
    return new Promise((resolve, reject) => {
      const successCallback = () => {
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);

        reject(errData);
      };

      Http.post(`activity-reports/${id}/verify`, {}, successCallback, errorCallback);
    });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
