export default {
  namespaced: true,

  state: {
    data: {
      username: '',
      full_name: '',
      role_id: 0,
    },
  },

  mutations: {
    setSource(state, source) {
      state.data = source;
    },
  },

  getters: {
    fullName: state => state.data.full_name,
  },
};
