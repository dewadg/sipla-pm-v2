import Http from '../services/Http';

const state = {
  data: [],
};

const mutations = {
  setSource(state, source) {
    state.data = source;
  },
};

const actions = {
  fetchAll(context) {
    return new Promise((resolve, reject) => {
      const successCallback = (res) => {
        context.commit('setSource', res.data);
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);

        reject(errData);
      };

      Http.get('permission-reports', successCallback, errorCallback);
    });
  },

  store(context, payload) {
    return new Promise((resolve, reject) => {
      const successCallback = () => {
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);

        reject(errData);
      };

      Http.post('permission-reports', payload, successCallback, errorCallback);
    });
  },

  update(context, payload) {
    return new Promise((resolve, reject) => {
      const { id } = payload;

      const successCallback = () => {
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);

        reject(errData);
      };

      Http.patch(`permission-reports/${id}`, payload, successCallback, errorCallback);
    });
  },

  delete(context, id) {
    return new Promise((resolve, reject) => {
      const successCallback = () => {
        resolve();
      };

      const errorCallback = (err) => {
        const errData = Object.assign({}, err.response);

        reject(errData);
      };

      Http.delete(`permission-reports/${id}`, successCallback, errorCallback);
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
