import Vue from 'vue';
import Vuex from 'vuex';
import LoggedUser from './LoggedUser';
import PermissionReport from './PermissionReport';
import ActivityReport from './ActivityReport';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    LoggedUser,
    PermissionReport,
    ActivityReport,
  },
});

export default store;
