import Vue from 'vue';
import VueRouter from 'vue-router';
import middleware from './middleware';

import HomeView from '../components/V2/Home/HomeView';
import TutorialView from '../components/Home/TutorialView';
import ContactView from '../components/V2/Contact/ContactView';
import LoginView from '../components/Login/LoginView';
import DashboardView from '../components/Dashboard/DashboardView';
import ReportView from '../components/Report/ReportView';

import PermissionReportIndex from '../components/PermissionReport/PermissionReportIndex';
import PermissionReportCreate from '../components/PermissionReport/PermissionReportCreate';
import PermissionReportEdit from '../components/PermissionReport/PermissionReportEdit';

import ActivityReportIndex from '../components/ActivityReport/ActivityReportIndex';
import ActivityReportCreate from '../components/ActivityReport/ActivityReportCreate';
import ActivityReportEdit from '../components/ActivityReport/ActivityReportEdit';

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    { name: 'index', path: '/', component: HomeView },
    { name: 'tutorial', path: '/tutorial', component: TutorialView },
    { name: 'contact', path: '/contact', component: ContactView },
    { name: 'login', path: '/login', component: LoginView },
    { name: 'dashboard', path: '/dashboard', component: DashboardView, beforeEnter: middleware.auth },    
    { name: 'reports.generator', path: '/report-generator', component: ReportView, beforeEnter: middleware.auth },
    { name: 'reports.permission_report_index', path: '/report/permission-reports', component: PermissionReportIndex, beforeEnter: middleware.auth },
    { name: 'reports.permission_report_create', path: '/report/permission-reports/new', component: PermissionReportCreate, beforeEnter: middleware.auth },
    { name: 'reports.permission_report_edit', path: '/report/permission-reports/:id', component: PermissionReportEdit, beforeEnter: middleware.auth },
    { name: 'reports.activity_report_index', path: '/report/activity-reports', component: ActivityReportIndex, beforeEnter: middleware.auth },
    { name: 'reports.activity_report_create', path: '/report/activity-reports/new', component: ActivityReportCreate, beforeEnter: middleware.auth },
    { name: 'reports.activity_report_edit', path: '/report/activity-reports/:id', component: ActivityReportEdit, beforeEnter: middleware.auth },
  ],
});

export default router;
