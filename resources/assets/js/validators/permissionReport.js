import { required, requiredIf, maxLength } from 'vuelidate/lib/validators';

export default {
  quarter: {
    required,
  },
  year: {
    required,
  },
  company_name: {
    required,
  },
  company_type_id: {
    required,
  },
  company_phone: {
    required,
    maxLength: maxLength(12),
  },
  company_address: {
    required,
  },
  business_field: {
    required,
  },
  sector_id: {
    required,
  },
  location: {
    required,
  },
  city_id: {
    required,
  },
  principle_permit_number: {
    required: requiredIf(model => model.report_type_id === 1),
  },
  business_permit_number: {
    required: requiredIf(model => model.report_type_id === 2),
  },
  amount_of_investment: {
    required,
  },
  national_labors: {
    required,
  },
  foreign_labors: {
    required,
  },
  date_of_approval: {
    required,
  },
};
