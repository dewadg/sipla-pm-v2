import { required } from 'vuelidate/lib/validators';

export default {
  quarter: {
    required,
  },
  year: {
    required,
  },
  company_name: {
    required,
  },
  date_of_registration: {
    required,
  },
  company_type_id: {
    required,
  },
  business_field: {
    required,
  },
  sector_id: {
    required,
  },
  city_id: {
    required,
  },
  permit_number: {
    required,
  },
  additional_realization: {
    required,
  },
  total_realization: {
    required,
  },
  national_labors: {
    required,
  },
  foreign_labors: {
    required,
  },
};
